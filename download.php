<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <meta charset="UTF-8">
    <title>下载 - 橡皮音乐</title>
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        .container {
            min-height: 100vh;
            display: flex;
            align-items: center;
            justify-content: center;
            height: 100%;
        }

        .box {
            background-color: #9e9e9eab;
            padding: 10% 20%;
            border-radius: 30px;
            line-height: 30px;
        }

        .background-image {
            background-size: cover;
            left: -10px;
            right: -10px;
            top: -10px;
            bottom: -10px;
            position: fixed;
            z-index: -1;
            filter: blur(5px);
        }
    </style>
</head>
<body>
<div class="background-image"
     style="background-image: url('http://cn.bing.com//th?id=OHR.RainbowMountain_ZH-CN9670076890_1920x1080.jpg&amp;rf=LaDigue_1920x1080.jpg&amp;pid=hp');"></div>
<?php
$name = $_GET['name'];
$author = $_GET['author'];
$url = $_GET['url'];

$param = $_SERVER["QUERY_STRING"];
$url_pos = strpos($param, $url);
$url = substr($param, $url_pos);
?>
<div class="container">
    <div class="box">
        <h1>橡皮音乐下载器</h1>
        <div>歌曲：《<?php echo $name; ?>》</div>
        <div>演唱：<?php echo $author; ?></div>
        <div>下载地址：<a href="<?php echo $url; ?>">点击下载</a></div>
    </div>
</div>
</body>
</html>