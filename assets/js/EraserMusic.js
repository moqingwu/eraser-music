console.log("%c2021 ©lifankohome EraserMusic-v1.0.0 OpenSource: https://gitee.com/lifanko/eraser-music", "color:#F40;background-color:#ccc;padding:5px;border-radius:2px;")

function log(msg) {
    const DEBUG = false
    if (DEBUG) {
        console.log(msg)
    }
}

function resize() {
    let width = window.innerWidth
    let height = window.innerHeight;
    let ele = document.getElementById("image");
    let size = height < width ? width : height
    let size_extend = 1.1 * size
    let height_offset = height < width ? (width - height) / 2 : 0
    let width_offset = width < height ? (height - width) / 2 : 0;

    ele.style.width = size_extend + "px"
    ele.style.height = size_extend + "px"
    ele.style.marginLeft = .05 * -size - width_offset + "px"
    ele.style.marginTop = .05 * -size - height_offset + "px"
    ele.style.backgroundSize = size_extend + "px " + size_extend + "px"
}

window.onresize = function () {
    resize()
};

// Audio object
const player = new Audio();
player.autoplay = true

// mTitle
const mTitle = new MTitle(document.title);

// auto play after switch song
let auto_play = false
// cookies timeout
const cookie_timeout = 30
// fall back audio
const fall_back = 'assets/audio/FAIL.mp3'
// vue
const app = new Vue({
    el: '#eraser_music',
    data: {
        version: {
            player: '1.2.2'
        },
        visits: 0,
        counter: '00:00',
        uid: '欢迎使用橡皮音乐',
        menu: {
            a: 'inactive',
            b: 'inactive',
            c: 'active'
        },
        volume: 100,
        keyword: '',
        searching: {},
        pending: false,
        search_param: {
            hot: [],
            history: []
        },
        history: [],
        list: [],
        wipe_time: {
            history_search: 0,
            history_play: 0
        },
        present: {
            album: 'https://cdn.lifanko.cn/img/nocover.jpg',
            past: '-:--',
            left: '-:--',
            total: 0,
            status: 'loading',
            progress: '0%'
        },
        LYRIC: {}
    },
    methods: {
        init: function () {
            // set and save uid in cookies
            let uid = getCookie('eraser_music');
            if (uid === null) {
                uid = getRandomString(24);
                setCookie("eraser_music", uid, cookie_timeout);
            }
            app.uid = uid
            // set and save volume in cookies
            let volume = getCookie('eraser_music-volume');
            if (volume === null) {
                volume = 100;
                setCookie("eraser_music-volume", volume, cookie_timeout);
            }
            app.volume = volume
            // update volume display
            this.set_volume()
            // get params from backend
            $.get("api.php", {
                o: 'init',
                u: uid
            }, function (res) {
                res = eval("(" + res + ")")
                if (app.check(res) === false) {
                    return;
                }

                // Version & Cnt
                app.version.backend = res.data.version.backend
                app.version.Meting = res.data.version.Meting
                app.visits = res.data.visits

                app.history = res.data.history
                app.list = res.data.list
                app.search_param = res.data.search_param
                // TODO optimise: remove or jump out
                app.searching = res.data.search_param.platform
                for (let key in app.searching) {
                    if (app.searching.hasOwnProperty(key)) {
                        app.searching = {
                            platform: key,
                            msg: ''
                        }
                    }
                    break;
                }
                // register player callback
                init_player()
                // player load first song in list, no auto play
                set_present(0)

                // adjust background
                resize()
            })
        },
        sw_menu: function (index) {
            // switch menu by index
            if (app.menu[index] === 'active') {
                return;
            }
            app.menu = {
                a: 'inactive',
                b: 'inactive',
                c: 'inactive'
            }
            app.menu[index] = 'active'
        },
        play: function () {
            // auto play&pause according player status
            let status = app.present.status
            if (status === 'ready' || status === 'pause') {
                player.play()
            } else {
                player.pause()
            }
        },
        prev: function () {
            // previous song

            // set status to pause
            app.present.status = 'pause'
            // auto play after select next song
            auto_play = true

            // get present index
            let index = present_index()
            // jump to previous or return to list bottom
            if (index === 0) {
                index = app.list.length - 1
            } else {
                index -= 1
            }
            // start loading this song
            this.confirm(index)
        },
        next: function () {
            // next song

            // set status to pause
            app.present.status = 'pause'
            // auto play after select next song
            auto_play = true
            // get present index
            let index = present_index()
            log(index)
            // jump to next or return to list top
            if (index === app.list.length - 1) {
                index = 0
            } else {
                index++
            }
            // start loading this song
            this.confirm(index)
        },
        select: function (id) {
            // select song while click a song

            // auto_play make song play automatic after selected complete
            auto_play = true
            if (id === app.present.id) {
                // if song has been selected, perform play and pause
                this.play()
                return;
            }
            // get list index by music id
            let index = present_index(id)
            // start loading this song
            this.confirm(index)
        },
        add: function (index) {
            // add song from history

            // get insert item
            let item = app.history[index]
            // if list exist this song, play this and not add
            if (app.present.id !== undefined && item.id !== undefined) {
                let exist = app.list.every(item_list => {
                    if (item_list.id === item.id) {
                        // play this song exist in list
                        this.select(item.id)

                        return false
                    }
                    log(item.id)
                    return true
                })
                if (exist === false) {
                    // stop add song to list
                    log('exist')
                    return;
                }
            }
            // get present list's index
            let idx = present_index() + 1
            // add song after present song: index+1 (present song is NG cause add this song)
            app.list.splice(idx, 0, item)
            // play this song
            this.confirm(idx)
        },
        confirm: function (index) {
            // select song by index of list

            // parsing start, show tip on the right of the list head
            app.pending = true
            // pause player after change song
            player.pause()
            // player controller status
            app.present.status = 'loading'
            // check if url has been parsed
            if (app.list[index].url === 'FAIL') {
                tip("歌曲加载失败，1秒后自动切换下一曲", "50%", 1000, "1", true);

                // set fall back
                app.list[index].url = fall_back
                app.list[index].lyric = false

                // play this item
                set_present(index)
            } else if (app.list[index].url.toString().substr(0, 4) === 'http') {
                // play this item
                set_present(index)
                // add history
                app.history.push({
                    id: app.present.id,
                    name: app.present.name,
                    author: app.present.author,
                    album: app.present.album,
                    url: app.present.url,
                    lyric: app.present.lyric,
                    timestamp: '新增'
                })
            } else {
                // parse url,lyric (album has been loaded while searching, or album in the list can't show)
                log('pending')

                if (app.list[index].url === fall_back) {
                    tip("歌曲加载失败，自动切换下一曲", "50%", 1000, "1", true);
                    // play fall back item which has been set up
                    set_present(index)
                    return;
                }

                $.get("api.php", {
                    u: app.uid,
                    o: 'parse',
                    p: app.list[index].platform,
                    n: app.list[index].name,
                    t: app.list[index].author,
                    a: app.list[index].album,
                    r: app.list[index].url,
                    l: app.list[index].lyric,
                    s: app.list[index].id
                }, function (res) {
                    res = eval("(" + res + ")")
                    if (app.check(res) === false) {
                        return;
                    }

                    // jump over if song can't parse
                    if (res.data.url === 'FAIL') {
                        tip("歌曲加载失败，自动切换下一曲", "50%", 1000, "1", true);
                        // set fall back item
                        app.list[index].url = fall_back
                        app.list[index].lyric = false
                        // play this item
                        set_present(index)
                    } else {
                        // set list
                        app.list[index].url = res.data.url
                        app.list[index].lyric = res.data.lyric
                        // play this item
                        set_present(index)
                        // add history
                        if (app.history.length === 0 || app.history[app.history.length - 1].id !== app.present.id) {
                            app.history.push({
                                id: app.present.id,
                                name: app.present.name,
                                author: app.present.author,
                                album: app.present.album,
                                url: app.present.url,
                                lyric: app.present.lyric,
                                timestamp: '新增'
                            })
                        }
                    }
                })
            }
        },
        set_volume: function () {
            // save volume in cookies (Range: 0-100)
            setCookie("eraser_music-volume", app.volume, cookie_timeout);
            // player volume accept range: 0-1
            player.volume = app.volume / 100
        },
        search: function (platform) {
            // trim keyword
            app.keyword = app.keyword.trim()
            if (app.keyword.length === 0) {
                // show tip if input error
                this.search_tip('请输入关键词后进行搜索', 1000)
                return;
            } else {
                this.search_tip('正在 ' + app.search_param.platform[platform] + ' 中搜索')
            }

            $.get("api.php", {
                u: app.uid,
                o: 'search',
                k: app.keyword,
                p: platform
            }, function (res) {
                // add history local directly
                app.search_param.history.unshift(app.keyword)
                // show search buttons
                app.searching.msg = ''
                // parse data
                res = eval("(" + res + ")")
                if (app.check(res) === false) {
                    return;
                }

                if (res.data.length > 0) {
                    app.list = res.data
                    // switch menu to list
                    app.menu = {
                        a: 'inactive',
                        c: 'active'
                    }
                    // always record platform while searching, to query song detail as param
                    app.searching.platform = platform
                    // play the first song of the list
                    app.select(app.list[0].id)
                } else {
                    tip("未搜索到相关歌曲", "50%", 3000, "1", true);
                }
            })
        },
        err_album: function (key = null, index = null) {
            // fill album that load error with default img
            if (key === null && index === null) {
                app.present.album = 'https://cdn.lifanko.cn/img/nocover.jpg'
            } else {
                app[key][index].album = 'https://cdn.lifanko.cn/img/nocover.jpg'
            }
        },
        set_keyword: function (keyword) {
            app.keyword = keyword
        },
        search_tip: function (msg, timeout = 0) {
            // show tip in search page with timeout
            app.searching.msg = msg

            if (timeout > 0) {
                setTimeout(function () {
                    app.searching.msg = ''
                }, timeout)
            }
        },
        download: function () {
            // open new window tab to download music
            window.open("download.php?name=" + app.present.name + '&author=' + app.present.author + '&url=' + app.present.url)
        },
        wipe: function (key) {
            const MAX = 3
            let timer = null

            // double click ele to perform wipe function less than 3s
            if (app.wipe_time[key] === 0) {
                app.wipe_time[key] = MAX
                timer = setInterval(function () {
                    if (--app.wipe_time[key] === 0) {
                        clearInterval(timer)
                        timer = null
                    }
                }, 1000)
            } else {
                log('wipe')
                // TODO optimise: clearInterval(timer), how to get the value of timer

                // clear local data
                if (key === 'history_search') {
                    app.search_param['history'] = []
                } else if (key === 'history_play') {
                    app.history = []
                }

                // clear backend data
                $.get("api.php", {
                    u: app.uid,
                    o: 'wipe',
                    k: key
                }, function () {
                    // ignore
                })
            }
        },
        check: function (res) {
            // error tip
            if (res.code !== 1) {
                tip('读取数据失败：' + res.msg, "50%", 3000, "1", true);
                return false;
            }
        }
    }
})
// initial eraser music
app.init();

// register callback
function init_player() {
    player.onplay = function () {
        log('playing')
        app.present.status = 'playing'

        // modify title
        mTitle.newTitle('正在播放《' + app.present.name + '》-' + app.present.author + '【橡皮音乐】');
        mTitle.start();
    }
    player.onpause = function () {
        log('pause')
        app.present.status = 'pause'

        mTitle.newTitle('已暂停：' + app.present.name + ' - ' + app.present.author + '【橡皮音乐】');
        mTitle.stop(true);
    }
    player.oncanplay = function () {
        log('ready')
        app.present.status = 'ready'
        // auto play
        if (auto_play) {
            player.play()
            player.auto_play = false
        }
        // init lyric
        index_lyric(0)
    }
    player.onload = function () {
        log('loading')
        app.present.status = 'loading'
    }
    player.onloadeddata = function () {
        app.present.total = player.duration
        app.present.past = '0:00'
        app.present.left = '-' + sec2time(app.present.total)
    }
    player.onended = function () {
        app.next()
    }
    player.ontimeupdate = function () {
        app.present.past = sec2time(player.currentTime)
        app.present.left = '-' + sec2time(app.present.total - player.currentTime)
        app.present.progress = (player.currentTime / app.present.total * 100).toFixed(2) + '%'
        // update lyric
        index_lyric(player.currentTime)
    }
    player.onerror = function () {
        // delete history that unable play
        app.history.pop()
        // show tip
        tip("歌曲无法播放，3秒后自动切换下一曲", "50%", 3000, "1", true);
        setTimeout(function () {
            app.next()
        }, 3000)
    }
}

// load LIST data of INDEX to PRESENT data & set player src so that start loading resource
function set_present(index) {
    if (app.list.length === 0) {
        return;
    }

    // parsing complete
    app.pending = false

    app.present.id = app.list[index].id
    app.present.name = app.list[index].name
    app.present.author = app.list[index].author
    app.present.album = app.list[index].album
    app.present.url = app.list[index].url

    // parse lyric
    app.present.lyric = parse_lyric(app.list[index].lyric)

    player.src = app.present.url
    log('set ' + index)
}

function parse_lyric(lyric) {
    log('parse lyric')
    if (lyric === false) {
        app.LYRIC.resource = [{
            timestamp: 0,
            text: '歌词加载失败'
        }]
        return;
    }

    let lyric_group = Array.from(lyric.matchAll(/\[(\d{2}:\d{2}\.\d{2,3})\](.*)/g))
    if (lyric_group.length) {
        app.LYRIC = {
            lines: lyric_group.length,
            current: 0,
            timestamp: 0,
            style: 'top: 1rem',
            resource: []
        }
        lyric_group.forEach(function (value) {
            app.LYRIC.resource.push({
                timestamp: parseInt(value[1].substr(0, 2)) * 60 + parseFloat(value[1].substr(3, 9)) - 0.5,
                text: value[2]
            })
        })
    } else {
        lyric_group = Array.from(lyric.matchAll(/(.*)\n/g))
        if (lyric_group.length) {
            app.LYRIC = {
                lines: lyric_group.length,
                current: 0,
                timestamp: 0,
                style: '',
                resource: []
            }
            lyric_group.forEach(function (value) {
                app.LYRIC.resource.push({
                    timestamp: 0,
                    text: value[1]
                })
            })
        }
    }
}

// set lyric focus by timestamp
function index_lyric(timestamp) {
    if (app.LYRIC.lines === 0 || app.LYRIC.style === '') {
        return;
    }
    // reset only when timestamp bigger than present
    if (timestamp >= app.LYRIC.timestamp) {
        // cancel last active line
        if (app.LYRIC.current >= 1) {
            app.LYRIC.resource[app.LYRIC.current - 1].active = false
        }
        // set active line
        app.LYRIC.resource[app.LYRIC.current].active = true
        // update next info
        if (app.LYRIC.current < app.LYRIC.lines - 1) {
            app.LYRIC.current++;
            app.LYRIC.timestamp = app.LYRIC.resource[app.LYRIC.current].timestamp
        }

        let current = app.LYRIC.current
        let offset = current < 10 ? 0 : current - 10
        app.LYRIC.style = 'top: -' + (offset * 1.5) + 'rem;margin-bottom: -' + (offset * 1.5) + 'rem'
    }
}

// get the index of list base on music-id
function present_index(id = null) {
    if (id === null) {
        id = app.present.id
    }

    let list_index = false
    app.list.forEach((value, index) => {
        if (value.id === id) {
            list_index = index
            // TODO optimise: jump out
        }
    })
    return list_index
}

// convert second to player past&left time
function sec2time(seconds) {
    let sec = Math.floor(seconds % 60)
    if (sec < 10) {
        sec = '0' + sec
    }
    return Math.floor(seconds / 60) + ':' + sec
}

// set cookie with key and expire
function setCookie(cookieKey, cookieValue, expireDays) {
    let expDate = new Date();
    expDate.setDate(expDate.getDate() + expireDays);
    //noinspection JSDeprecatedSymbols
    document.cookie = cookieKey + "=" + escape(cookieValue) +
        ((expireDays == null) ? "" : "; expires=" + expDate.toGMTString());
}

// get cookie with key
function getCookie(cookieKey) {
    let arr, reg = new RegExp("(^| )" + cookieKey + "=([^;]*)(;|$)");
    //noinspection JSDeprecatedSymbols
    return (arr = document.cookie.match(reg)) ? unescape(arr[2]) : null;
}

// get string combine with random char
function getRandomString(len) {
    len = len || 32;
    let $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let maxPos = $chars.length;
    let pwd = '';
    for (let i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
}